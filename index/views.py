import datetime
from django.contrib.auth import authenticate
from django.shortcuts import render, HttpResponse, redirect
from .models import News
from django.core.paginator import Paginator
from django.contrib.sessions.models import Session
from django.contrib.auth.models import User
def index(request):
    print(request.method)
    cont = {'hide':'False'}
    if User.is_authenticated:
        print('authed')
        print(f'Username:{User.username}')
        cont['hide']='True'
    else:
        cont['hide']='False'
    print(cont)
    return render(request, template_name='index.html', context=cont)


def newss(request):
    news = News.objects.order_by('published')
    paginator = Paginator(news, 5)
    page_number = request.GET.get('page')
    page = paginator.get_page(page_number)
    context = {'news':page}
    return render(request, 'news.html', context=context)


def createnews(request):
    if request.method != 'POST':
        return render(request, 'createnews.html')
    else:
        label = request.POST.get('label')
        date = datetime.datetime.now()
        text = request.POST.get('text')
        nNews = News()
        nNews.text = text
        nNews.pub_date = str(date)
        nNews.label = label
        nNews.published = False
        nNews.save()
        return render(request, "createnews.html", {'sended': 'НОВОСТЬ ОТПРАВЛЕНА'})


def about(request):
    if request.method == 'GET':
        return render(request, 'about.html')
    else:
        return HttpResponse('POST REQUEST NOT ALLOWED HERE')


def login(request):
    if request.method == 'POST':
        name = request.POST.get('nick')
        passw = request.POST.get('passw')
        print(name)
        print(passw)
        user = authenticate(username=name, password=passw)
        if user is not None:
            return redirect('/')
        else:
            return render(request, 'login.html', context={'extra':'пользователь не найден'})
    elif request.method == 'GET':
        return render(request, 'login.html')

def register(request):
    if request.method == 'GET':
        return render(request, 'reg.html')
    else:
        try:
            name = request.POST.get('nick')
            email = request.POST.get('email')
            passw = request.POST.get('passw')
            user = User.objects.create_user(username=name, email=email, password=passw)
            user.save()
            cont = {'REG':'Успешно зарегестрированно.'}
            return render(request, 'reg.html', context=cont)
        except Exception as e:
            msg = str(e)
            print(msg)
            namee =request.POST.get('nick')
            if str(e)== f'duplicate key value violates unique constraint "auth_user_username_key" DETAIL: Key (username)=({namee}) already exists.':
                cont = {'ERROR':'Такой пользователь уже существует'}
                return render(request, 'reg.html', context=cont)
            else:
                return HttpResponse(msg)
def test(request):
    return render(request, 'test.html')