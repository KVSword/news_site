from django.apps import AppConfig
from django.apps import AppConfig
from django.contrib.admin.checks import check_admin_app, check_dependencies
from django.core import checks
from django.utils.translation import gettext_lazy as _

class IndexConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name='index'
    verbose_name = _("Main")