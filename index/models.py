from django.db import models


class News(models.Model):
    label = models.TextField(max_length=255)
    text = models.TextField(max_length=1000)
    pub_date = models.DateTimeField(default='01.01.1970')
    published = models.BooleanField(default=False)

class theme(models.Model):
    name = models.TextField(max_length=100)
    description = models.TextField(max_length=500)