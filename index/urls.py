from django.urls import *
from django.http import request
from . import views
urlpatterns=[
    path('', views.index, name='index'),
    path('news', views.newss, name='news'),
    path('createnews', views.createnews, name='createnews'),
    path('login', views.login, name='login'),
    path('about', views.about, name='about'),
    path('register', views.register, name='register'),
    path('test', views.test, name='test')
]